-- chikun :: 2014
-- Manages different states


-- Create state table
state = {
    current = states.template
}


-- Kill old state and load a new one
function state.change(newState)

    state.current:kill()

    state.load(newState)

end


-- Loads new state or reloads current state if argument omitted
function state.load(newState)

    state.current = newState or state.current
    state.current:create()

end


-- Update current state
function state.update(dt)

    state.current:update(dt)

end


-- Draw current state
function state.draw(dt)

    state.current:draw()

end
