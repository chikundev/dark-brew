-- chikun :: 2014
-- Loads all graphics from the /gfx folder


-- Recursively checks a folder for graphics
-- and adds any found to a gfx table
function checkFolder(dir, tab)

    local tab = ( tab or { } )

    -- Get a table of files / subdirs from dir
    local items = f.getDirectoryItems(dir)

    for key, val in ipairs(items) do

        if f.isFile(dir .. "/" .. val) then
            -- If item is a file, then load it into tab

            -- Remove ".png" extension on file
            local name = val:gsub(".png", "")

            -- Load image into table
            tab[name] = g.newImage(dir .. "/" .. val)

        else
            -- Else, run checkFolder on subdir

            -- Add new table onto tab
            tab[val] = { }

            -- Run checkFolder in this subdir
            checkFolder(dir .. "/" .. val, tab[val])

        end

    end

    return tab

end


-- Load the gfx folder into the gfx table
gfx = checkFolder("gfx")

-- Kills checkFolder
checkFolder = nil
