-- chikun :: 2014
-- Core routines


core = { }      -- Create core table


function core.load()

    -- Load global variables required

    -- Load first map
    map.current = map.load("newMap")

end


function core.update(dt)

    -- Updated global variables

end


function core.draw()

    map.draw()

    -- Draw overlay

end
